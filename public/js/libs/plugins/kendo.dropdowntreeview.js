/// <version>2013.04.14</version>
/// <summary>Works with the Kendo UI 2013 Q1 and jQuery 1.9.1</summary>
/// Modified to work with require.js and grunt production build process
/// Modified to work with AMD definition i.e. custom kendo build - shaved 1000kb off of the build
/// however kendo is not defined in this implementation so we get it from window
//define(['kendo', 'jquery'],
//(function (kendo, $) {
define(['kendo/kendo.dropdownlist', 'kendo/kendo.treeview', 'jquery'],
    (function (ddl,tv,$) {

    //http://docs.telerik.com/kendo-ui/howto/create-custom-kendo-widget
    var kendo = window.kendo,
        ui = kendo.ui,
        Widget = ui.Widget;

    var ExtDropDownTreeView = kendo.ui.Widget.extend({

        /// <summary>
        /// Combine the DropDownList and TreeView widgets to create a DropDownTreeView widget.
        /// </summary>
        /// <author>John DeVight</author>

        _uid: null,
        _treeview: null,
        _dropdown: null,

        init: function (element, options) {
            /// <summary>
            /// Initialize the widget.
            /// </summary>

            var that = this;

            kendo.ui.Widget.fn.init.call(that, element, options);

            // Generate a unique id.
            that._uid = new Date().getTime();

            // Append the html to the "root" element for the DropDownList and TreeView widgets.
            $(element).append(kendo.format("<input id='extDropDown{0}' class='k-ext-dropdown'/>", that._uid));
            $(element).append(kendo.format("<div id='extTreeView{0}' class='k-ext-treeview' style='z-index:1;'/>", that._uid));

            // Create the dropdown.
            that._dropdown = $(kendo.format("#extDropDown{0}", that._uid)).kendoDropDownList({
                dataSource: [{ text: "", value: "" }],
                dataTextField: "text",
                dataValueField: "value",
                open: function (e) {
                    //to prevent the dropdown from opening or closing. A bug was found when clicking on the dropdown to 
                    //"close" it. The default dropdown was visible after the treeview had closed.
                    e.preventDefault();
                    // If the treeview is not visible, then make it visible.
                    if (!$treeviewRootElem.hasClass("k-custom-visible")) {
                        // Position the treeview so that it is below the dropdown.
                        $treeviewRootElem.css({
                            "top": $dropdownRootElem.position().top + $dropdownRootElem.height(),
                            "left": $dropdownRootElem.position().left
                        });
                        // Display the treeview.
                        $treeviewRootElem.slideToggle('fast', function () {
                            that._dropdown.close();
                            $treeviewRootElem.addClass("k-custom-visible");
                        });
                    }
                },
                close: function(e){
                    //alert('close');
                    console.log('kendo.dropdowntreeview:close');
                }
            }).data("kendoDropDownList");


            // If a width has been provided, then set the new width.
            if (options.dropDownWidth) {
                that._dropdown._inputWrapper.width(options.dropDownWidth);
            }

            var $dropdownRootElem = $(that._dropdown.element).closest("span.k-dropdown");

            // Create the treeview.
            that._treeview = $(kendo.format("#extTreeView{0}", that._uid)).kendoTreeView(options.treeview).data("kendoTreeView");
            that._treeview.bind("select", function (e) {

                // Based on changes to the workflow disabled nodes can be selected and their children displayed
                // The status of the text indicates what nodes meet the filter criteria


                // Check if the node has a disabled class - if so do nothing
                //if ($(e.node).find("div").hasClass("k-disabled-node")){
                //    //alert('node has disabled class');
                //    return;
                //}

                // When a node is selected, display the text for the node in the dropdown and hide the treeview.
                $dropdownRootElem.find("span.k-input").text($(e.node).children("div").text());
                $treeviewRootElem.slideToggle('fast', function () {
                    $treeviewRootElem.removeClass("k-custom-visible");
                    that.trigger("select", e);
                });

                //alert('trigger: close');
                that._dropdown.trigger("close", e);
            });

            var $treeviewRootElem = $(that._treeview.element).closest("div.k-treeview");

            // Hide the treeview.
            $treeviewRootElem
                .width($dropdownRootElem.width())
                .css({
                    "border": "1px solid grey",
                    "display": "none",
                    "position": "absolute",
                    "background-color": that._dropdown.list.css("background-color")
                });

            $(document).click(function (e) {
                // Ignore clicks on the treetriew.
                if ($(e.target).closest("div.k-treeview").length == 0) {
                    // If visible, then close the treeview.
                    if ($treeviewRootElem.hasClass("k-custom-visible")) {
                        $treeviewRootElem.slideToggle('fast', function () {
                            $treeviewRootElem.removeClass("k-custom-visible");
                            that._dropdown.trigger("close", e);
                        });
                    }
                }
            });
        },

        dropDownList: function () {
            /// <summary>
            /// Return a reference to the DropDownList widget.
            /// </summary>

            return this._dropdown;
        },

        treeview: function () {
            /// <summary>
            /// Return a reference to the TreeView widget.
            /// </summary>

            return this._treeview;
        },

        value: function(value) {

            // Clear the dropdown
            this._dropdown.text("");
            // Unselect the value in the tree
            this._treeview.select() //gets currently selected <li> element
                .find("span.k-state-selected")
                .removeClass("k-state-selected"); //removes the highlight class
        },

        options: {
            name: "ExtDropDownTreeView"
        }
    });
    kendo.ui.plugin(ExtDropDownTreeView);
    return ExtDropDownTreeView;

}));