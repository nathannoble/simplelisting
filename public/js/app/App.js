define(['jquery', 'backbone', 'marionette', 'underscore', 'handlebars', 'regions/TransitionRegion', 'regions/ModalRegion', 'models/AppModel',
        'models/AppUtilityModel', 'utilities/AppStandards', 'enums/AppEnums', 'events/AppEvents',
        'utilities/LayoutDebugger', 'utilities/NumberUtilities', 'views/ErrorView','options/ViewOptions','get-shit-done'], //,'adminLTE','bootstrap','jquery.slimScroll','fastclick','adminLTE'
    function ($, Backbone, Marionette, _, Handlebars, TransitionRegion, ModalRegion, AppModel, AppUtilities, AppStandards,
              AppEnums, AppEvents, LayoutDebugger, NumberUtilities, ErrorView, ViewOptions) {

        if ($.fn.tooltip.noConflict) {
            /*** Handle jQuery plugin naming conflict between jQuery UI and Bootstrap ***/
            var bootstrapTooltip = $.fn.tooltip.noConflict();
            $.fn.bstooltip = bootstrapTooltip;
        }

        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined' ? args[number] : match;
                });
            };
        }

        // Text excision for generating middle ellipsis values to fit a specific size.
        String.prototype.cutMiddleChar = function() {
            var charPosition = Math.floor(this.length / 2);
            return this.substr(0, charPosition) + this.substr(charPosition + 1);
        };
        String.prototype.insertMiddleEllipsis = function() {
            var charPosition = Math.floor(this.length / 2);
            return this.substr(0, charPosition) + '...' + this.substr(charPosition);
        };

        // Declare functions
        function isMobile() {
            var userAgent = navigator.userAgent || navigator.vendor || window.opera;
            return ((/iPhone|iPod|iPad|Android|BlackBerry|Opera Mini|IEMobile/).test(userAgent));
        }

        function isPhone() {
            if (isMobile){
                var size = window.getComputedStyle(document.body,':after').getPropertyValue('content');
                if (size.indexOf('smallscreen') != -1)
                    return true;
            }

            return false;
        }

        function setupLayoutNotSupported(){
            // Remove scrolling
            document.body.scrollTop = document.documentElement.scrollTop = 0;
            document.ontouchmove = function(e){ e.preventDefault(); };
            $("body").css('overflow', 'hidden');

            // Display orientation not supported screen
            $("#orientation-not-supported").css('display', 'block');

            // Hide other containers
            $("#loading-error-region").css('display', 'none');
            $("#sl").css('visibility', 'hidden');
        }

        function setupLayoutSupported(){
            // Restore scrolling
            document.ontouchmove = function(e){ return true; };
            $("body").css('overflow', '');

            // Hide the orientation not supported screen
            $("#orientation-not-supported").css('display', 'none');

            // Restore other containers
            $("#loading-error-region").css('display', 'none');

            $("#sl").css('visibility', '');
        }

        function doConditionalLayout(landscape){
            console.log('App:doConditionalLayout');

            if (!App.mobile && !App.admin)
                return;

            if (App.phone){
                if (landscape){
                    //setupLayoutNotSupported();
                    setupLayoutSupported();
                }
                else{
                    setupLayoutSupported();
                }
            }
            else{
                if (landscape){
                    setupLayoutSupported();
                }
                else{
                    setupLayoutNotSupported();
                }
            }

        }

        // Create an instance of the application
        var App = new Backbone.Marionette.Application();

        // Create related utilities object - this could be a module, generic object etc. etc.
        // TODO: Utilities should inherit from Class i.e. overhead of model is not required
        App.utilities = new AppUtilities();
        App.standards = new AppStandards(App);
        App.layoutDebugger = new LayoutDebugger();

        // Load up the data source URL
        $.support.cors = true;
        //var dataServiceURL = "http://localhost:63554/rest/GetConfiguration";
        var dataServiceURL = null;

        //$.getJSON("config.json", function(json) {
        //    dataServiceURL = data.DataServiceURL;
        //});

        // Regions
        App.addRegions({
            //loadingErrorRegion:"#loading-error-region",
            headerRegion:"#header-region",
            //asideRegion:"#aside-region",
            mainRegion:"#main-region",
            //footerRegion:"#footer-region",
            modal: ModalRegion
        });

        $.ajax({
            type: 'GET',
            url: 'config.json',
            dataType: 'json',
            success: function (data) {
                dataServiceURL = data.DataServiceURL;
            },
            error: function(){
                var viewOptions = new ViewOptions();
                viewOptions.link = "config.json";
                viewOptions.fullScreen = true;
                $("body").css('overflow', 'hidden');
                App.loadingErrorRegion.show(new ErrorView(viewOptions));
            },
            data: {},
            async: false
        });


        App.config = {};
        App.config.DataServiceURL = dataServiceURL;

        // Setup events enum
        App.Events = {};
        App.Events = new AppEvents();
        //App.AdminEvents = new AdminAppEvents();

        // Enums
        App.Enums = {};
        App.Enums = new AppEnums();

        // Utilities
        App.Utilities = {};
        App.Utilities.numberUtilities = new NumberUtilities();

        // App Events
        $(window).on('orientationchange', function(){
            console.log('App:onOrientationChange');
        });

        // Debugging so we can see what screen layout we're in
        var winWidth = 0, winHeight = 0, appResizeTimeout;
        $(window).resize(function(){
            console.log('App:resize');

            // Get height and width
            var winNewWidth = $(window).width();
            var winNewHeight = $(window).height();

            // Compare the new height and width with old one
            if(winWidth !== winNewWidth || winHeight !== winNewHeight)
            {
                window.clearTimeout(appResizeTimeout);
                appResizeTimeout = window.setTimeout(appResize, 10);
            }
            //Update the width and height
            winWidth = winNewWidth;
            winHeight = winNewHeight;

        });

        function appResize(){
            console.log('App:appResize');

            var width = $(window).width();
            var height = $(window).height();

            if (width > height)
                doConditionalLayout(true);
            else
                doConditionalLayout(true);

            if (width <= 768)
                console.log('DEBUG: -------------------------------------- screen-xs - ' + width);
            else if (width > 768 && width <= 992)
                console.log('DEBUG: -------------------------------------- screen-sm - ' + width);
            else if (width >= 992 && width < 1200)
                console.log('DEBUG: -------------------------------------- screen-md - ' + width);
            else if (width >= 1200)
                console.log('DEBUG: -------------------------------------- screen-lg - ' + width);
        }

        // Debugging so we can see events flying around
        App.vent.on('all', function (evt, model) {
            console.log('***** DEBUG: Event Caught: ' + evt + ":" + model);
            //if (model) {
            //    console.dir(model);
            //}
        });

        // Catch ajax errors from anywhere in the app with ErrorView
        $(document).ajaxError(function (e, xhr, options) {

            // Ignore aborted calls
            if (xhr.statusText.toUpperCase() === "ABORT")
                return;

            console.log('App.modal:ErrorView:url:' + options.url);
            console.log(e);
            console.log(xhr);
            console.log(options);

            var url = options.url;
            var view = App.modal.currentView;

            // If alert has already been issued, don't show error region (i.e. admin photo page)
            //var alert = $(".alert");

            if ((!view || view === null)) { // && (alert.length === 0)) {

                var viewOptions = new ViewOptions();
                viewOptions.link = url;
                viewOptions.fullScreen = false;
                App.headerRegion.reset();
                //App.asideRegion.reset();
                App.mainRegion.reset();
                //App.footerRegion.reset();
                $("body").css('overflow', 'hidden');
                $("#sl").css('display', 'none');
                console.log('App.modal:ErrorView:show');
                App.loadingErrorRegion.show(new ErrorView(viewOptions));
            }
        });

        // Startup history
        App.addInitializer(function () {
            Backbone.history.start();
        });

        // Track mobile
        App.mobile = isMobile();

        // Track Small Screens
        App.phone = isPhone();

        // Here we are going to let an Application Singleton handle state and application events
        App.model = new AppModel(App);

        // Setup orientation
        appResize();

        return App;
    });