// Include Desktop Specific JavaScript files here (or inside of your Desktop Controller, or differentiate based off App.mobile === false)
require(["App", "routers/PhoneAppRouter", "controllers/PhoneController", "jquery", "backbone", "marionette", "jqueryui",
        "bootstrap", "backbone.validateAll"],
    function (App, PhoneAppRouter, PhoneController) {
        // Prevents all anchor click handling
        //$.mobile.linkBindingEnabled = false;

        // Disabling this will prevent jQuery Mobile from handling hash changes
        //$.mobile.hashListeningEnabled = false;

        App.router = new PhoneAppRouter({
            controller:new PhoneController()
        });

        App.start();
    });