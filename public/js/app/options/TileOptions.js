define(["jquery", "backbone",  "../custom/Class"],
    function($, Backbone, Class) {

        return Class.extend({
            initialize: function(){
                console.log('TileOptions:initialize');

            },
            addMapButton: false              // Let's the view know if it needs to add "map button" to the tile

        });
    });