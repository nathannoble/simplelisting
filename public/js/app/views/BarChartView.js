define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/barChart', 'kendo/kendo.data', 'chartjs',
        'jquery.dateFormat'],
    function (App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('BarChartView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

                this.dataSeriesLabels = [];
                this.dataSeriesIDs = [];
                this.dataSeries = [];

                var self = this;
                var app = App;

                this.dateFilter = App.model.get('selectedDateFilter');
                this.year = app.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter); //2016;
                this.month = app.Utilities.numberUtilities.getMonthFromDateFilter(this.dateFilter); //11;

            },
            onRender: function () {
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('---------- BarChartView:onShow ----------');

                // Create chart
                this.getData();

                this.$('#barChartTitle').html(this.options.type);
            },
            getData: function () {
                //console.log('BarChartView:getData');

                var self = this;
                var app = App;

                // Team Leaders

                //SELECT employeeID, firstName, Lastname
                //FROM tblProgressEmployees
                //WHERE isTeamLeader = 1
                //AND employeeID <> 74

                this.employeeDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                dataType: "json"
                            }
                            //update: {
                            //    url: function (data) {
                                    //console.log('BarChartView:employeeDataSource:update');
                            //        return App.config.DataServiceURL + "/odata/ProgressEmployee" + "(" + data.employeeID + ")";
                            //    }
                            //},
                            //create: {
                            //    url: function (data) {
                            //        //console.log('BarChartView:employeeDataSource:create');
                            //        return App.config.DataServiceURL + "/odata/ProgressEmployee";
                            //    }
                            //    //dataType: "json"
                            //},
                            //destroy: {
                            //    url: function (data) {
                            //        //console.log('BarChartView:employeeDataSource:destroy');
                            //        return App.config.DataServiceURL + "/odata/ProgressEmployee" + "(" + data.employeeID + ")";
                            //    }
                            //}
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "employeeID",
                                fields: {
                                    employeeID: {editable: false, type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    emailAddress: {editable: true, type: "string", validation: {required: true}},
                                    esaLocalPhone: {editable: true, type: "string", validation: {required: false}},
                                    eeTypeTitle: {type: "string", validation: {required: false}},
                                    status: {
                                        type: "string",
                                        validation: {required: false},
                                        values: [
                                            {text: '1 - Active', value: '1 - Active'},
                                            {text: '2 - Termed', value: '2 - Termed'},
                                            {text: '3 - No Hire', value: '3 - No Hire'}
                                        ]
                                    },
                                    isTeamLeader: {editable: true, type: "boolean"},
                                    isEligibleSupport: {editable: true, type: "boolean"},
                                    maxClientLoad: {editable: true, type: "number"},
                                    currentClientLoad: {editable: true, type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        sort: {field: "employeeID", dir: "asc"},
                        requestStart: function (e) {
                            ////console.log('BarChartView:dataSource:request start:');
                            kendo.ui.progress(self.$("#loadingChart"), true);
                        },
                        requestEnd: function (e) {
                            ////console.log('BarChartView:dataSource:request end:');
                            //var data = this.data();
                            //kendo.ui.progress(self.$("#loadingChart"), false);
                        },
                        error: function (e) {
                            ////console.log('PortalEmployeeGridView:dataSource:error');
                            kendo.ui.progress(self.$("#loadingChart"), false);
                        },
                        change: self.onEmployeeDataSourceChange
                    });

                //SELECT clientID, schedulerID, teamLeaderID, planHours, mtgTargetLow, mtgTargetHigh
                //FROM getClients
                //WHERE teamLeaderID = #getLeaders.employeeID#

                this.progressClientDataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/ProgressClient";
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "clientID",
                            //    "clientID":257,"schedulerID":0,"divisionID":null,"clientTimeZoneID":1,
                            //    "firstName":"Melanie","lastName":"Dunn","customerName":null,"companyClient":false,
                            //    "companyID":7,"planID":22,"hotList":1,"adminSchedulerID":0,"typeOfService":"1 - Scheduling",
                            //    "industry":"--- NONE ---","division":"","title":"","referredBy":"",
                            //    "lfdAuthorization":"N/A","dsuNumber":"","product":"--- NONE ---",
                            //    "distributionChannel":"--- NONE ---","states":"N CA - Reno","lengthInTerritory":"--- NONE ---",
                            //    "status":"6 - Termed","emailAddress":"melanie.dunn@genworth.com",
                            //    "workPhone":"(206) 979-1441","workExtension":"","address":"1550 Filbert Street #10",
                            //    "city":"San Fransico","state":"CA","zipCode":"94123","homePhone":"","mobilePhone":"",
                            //    "assistantInternal":"Kristi Mayhew","assistantPhone":"(804) 484-7002","assistantExtension":"",
                            //    "assistantEmail":"","realTimeAccess":false,"realTimeUser":"","realTimePassword":"",
                            //    "otherUsernames":"vOtherUserNames","esaAnniversary":"2010-06-14T00:00:00",
                            //    "termDate":"2011-01-07T00:00:00","sbCount":true,"additionalInformation":"","sysCrmUserName":"",
                            //    "sysCrmPassword":"","sysCrmNotes":"","sysEmailUserName":"","sysEmailPassword":"",
                            //    "sysEmailNotes":"","sysCalendar":"","sysCalendarUserName":"","sysCalendarPassword":"",
                            //    "sysCalendarNotes":"","taskTargetLow":15,"taskTargetHigh":20,"mtgTargetLow":3,"mtgTargetHigh":5
                            fields: {
                                clientID: {editable: false, defaultValue: 0, type: "number"},
                                status: {editable: true, type: "string", validation: {required: false}},
                                planHours: {type: "number"},
                                taskTargetLow: {type: "number"},
                                taskTargetHigh: {type: "number"},
                                mtgTargetLow: {type: "number"},
                                mtgTargetHigh: {type: "number"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   SummaryWidgetView:requestStart');

                        kendo.ui.progress(self.$("#loadingChart"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    SummaryWidgetView:requestEnd');
                        //kendo.ui.progress(self.$("#loadingChart"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   SummaryWidgetView:error');
                        kendo.ui.progress(self.$("#loadingChart"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: self.onProgressClientDataSourceChange
                });

                // *** data call - vProgressClientData ***

                // getTeamClientData
                // SELECT SUM(TotalMeetings) AS MeetingsTotal, SUM(TotalMeetingsWithComp)
                // AS MeetingsTotalWithComp FROM getClientData
                // WHERE clientID IN(341,403,432,443,486,1145,1774,1808,1845,1894,1897,1916,1931,1950,1953,1980,2011,2017,2066,2067,2068,2074,2087,2100,2122,2123,2126,2133,2134,2148,2209,2239,2249,2307,2312,2317,2355,2376,2379,2380,2382,2384,2393,2394,2402,2406,2479,2487,2488,2494,2563)


                //SELECT clientId, sumTotalTime, sumAdminTime, SumTotalTasks, sumTotalMeetings,
                //    sumCompTime, sumTotalTasksWithComp, sumTotalMeetingsWithComp
                //FROM vProgressClientData
                //WHERE vYear = #thisYear# AND vMonth = #thisMonth#
                //ORDER BY clientID

                //"clientID":381,"vYear":2010,"vMonth":7,"sumTotalTime":1.23,"sumTotalTasks":20,
                //"sumTotalMeetings":1,"sumAdminTime":0.0,"sumCompTime":0.0,"sumCompCalls":0,"sumCompMeetings":0,
                //"sumTotalTimeWithComp":1.23,"sumTotalTasksWithComp":20,"sumTotalMeetingsWithComp":1,"entryID":146

                this.dataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/ProgressClientData";
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },

                        //"clientID":257,"vYear":2010,"vMonth":7,"sumTotalTime":1.39,"sumTotalTasks":0,"sumTotalMeetings":0,
                        // "sumAdminTime":1.39,"sumCompTime":0.0,"sumCompCalls":0,"sumCompMeetings":0,"sumTotalTimeWithComp":1.39,
                        // "sumTotalTasksWithComp":0,"sumTotalMeetingsWithComp":0,"entryID":172

                        //"entryID":146, clientID":381,
                        // "vYear":2010,"vMonth":7,"sumTotalTime":1.23,"sumTotalTasks":20,
                        //"sumTotalMeetings":1,"sumAdminTime":0.0,"sumCompTime":0.0,"sumCompCalls":0,"sumCompMeetings":0,
                        //"sumTotalTimeWithComp":1.23,"sumTotalTasksWithComp":20,"sumTotalMeetingsWithComp":1,
                        model: {
                            id: "entryID",
                            fields: {
                                entryID: {editable: false, defaultValue: 0, type: "number"},
                                clientID: {editable: false, defaultValue: 0, type: "number"},
                                workDate: {editable: true, type: "date", validation: {required: false}},
                                timeStamp: {editable: true, type: "date", validation: {required: false}},
                                vYear: {type: "number"},
                                vMonth: {type: "number"},
                                sumTotalTime: {type: "number"},
                                sumTotalTasks: {type: "number"},
                                sumTotalMeetings: {type: "number"},
                                sumAdminTime: {type: "number"},
                                sumCompTime: {type: "number"},
                                sumCompCalls: {type: "number"},
                                sumCompMeetings: {type: "number"},
                                compMeetings: {type: "number"},
                                sumTotalTimeWithComp: {type: "number"},
                                sumTotalTasksWithComp: {type: "number"},
                                sumTotalMeetingsWithComp: {type: "number"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   BarChartView:requestStart');

                        kendo.ui.progress(self.$("#loadingChart"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    BarChartView:requestEnd');
                        //kendo.ui.progress(self.$("#loadingChart"), false);

                    },
                    error: function (e) {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   BarChartView:error:' + JSON.stringify(e));
                        kendo.ui.progress(self.$("#loadingChart"), false);

                        //self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    //<cfquery name="getTeamClientData" dbtype="query" maxrows="1">
                    //    SELECT SUM(TotalMeetings) AS MeetingsTotal, SUM(TotalMeetingsWithComp) AS MeetingsTotalWithComp
                    //    FROM   getClientData
                    //    WHERE  clientID IN(#vListClients#)
                    //</cfquery>
                    //WHERE    vYear = #thisYear# AND vMonth = #thisMonth#
                    filter: {
                        logic: "and",
                        filters: [
                            {field: "vYear", operator: "eq", value: self.year},
                            {field: "vMonth", operator: "eq", value: self.month}
                        ]
                    }
                    //change: self.onDataSourceChange
                });

                self.dataSource.fetch(function (data) {

                    var empState = {};
                    var empFilters = [];
                    empFilters.push({field: "isTeamLeader", operator: "eq", value: true});
                    empFilters.push({field: "employeeID", operator: "neq", value: 74});
                    empState.filter = empFilters;
                    self.employeeDataSource.query(empState);
                });



            },
            onEmployeeDataSourceChange: function (e) {
                var self = this;

                var empData = self.employeeDataSource.data();
                self.leaderCount = empData.length;

                //console.log('BarChartView:onEmployeeDataSourceChange:leaderCount:' + self.leaderCount);
                // For each leader returned by employeeDataSource
                var state = {};
                var filters = [];

                $.each(empData, function (index, value) {
                    //var record = value;
                    //self.leaderID = this.employeeID;
                    //self.firstName = this.firstName;

                    self.dataSeriesLabels.push("Team " + this.firstName);
                    self.dataSeriesIDs.push(this.employeeID);
                    //console.log('BarChartView:employeeDataSource:leaderID:firstName' + this.employeeID + ":" + this.firstName);

                    filters.push({field: "teamLeaderID", operator: "eq", value: this.employeeID});

                });
                //filters.push({field: "status", operator: "contains", value: "4"});

                state.filter = {
                    logic: "or",
                    filters: filters
                };
                self.progressClientDataSource.query(state);

            },
            onProgressClientDataSourceChange: function (e) {
                //console.log('BarChartView:onProgressClientDataSourceChange');

                var self = this;

                var data = this.progressClientDataSource.data();

                var clientRecords = [];

                //var clientIDs = [];
                //var teamLeaderID = 0;
                //var state = {};
                //var filters = [];

                //<cfquery name="getTargets" dbtype="query" maxrows="1">
                //    SELECT SUM(mtgTargetLow  * planHours) AS meetingsTarget
                //    FROM getTeamClients
                //</cfquery>

                $.each(data, function (index, value) {
                    var record = value;
                    if (record.status.indexOf("4") > -1) {
                        ////console.log('BarChartView:onProgressClientDataSourceChange:clientData record:' + JSON.stringify(record));
                        clientRecords.push({
                            clientID: record.clientID,
                            mtgTargetLow: record.mtgTargetLow,
                            planHours: record.planHours,
                            teamLeaderID: record.teamLeaderID,
                            status: record.status
                        });
                    }

                });

                self.parseDataSourceResults(clientRecords);
            },
            parseDataSourceResults: function (clientRecords) {

                var self = this;
                var app = App;

                var data = this.dataSource.data();
                var datarecord = data[0];
                //console.log('BarChartView:parseDataSourceResults:datarecord:' + JSON.stringify(datarecord));
                //console.log('BarChartView:parseDataSourceResults:clientRecords.length:' + clientRecords.length);

                // ProgressClientData records
                $.each(self.dataSeriesIDs, function (index3, value3) {

                    var sumTotalMeetings = 0;
                    var sumTotalMeetingsWithComp = 0;
                    var sumMeetingsTarget = 0;

                    var record3 = value3;

                    // ProgressEmployee records
                    $.each(clientRecords, function (index2, value2) {
                        var record2 = value2;
                        if (record2.teamLeaderID === record3) {

                            // ProgressClient records
                            $.each(data, function (index, value) {
                                var record = value;
                                clientID = value.clientID;
                                //if (clientIDs.indexOf(clientID) > 0) {
                                if (record2.clientID === clientID) {
                                    sumTotalMeetings += value.sumTotalMeetings;
                                    sumTotalMeetingsWithComp += value.sumTotalMeetingsWithComp;
                                }
                            });
                            sumMeetingsTarget += record2.mtgTargetLow * record2.planHours;
                        }
                    });

                    var mtgPercent = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumTotalMeetings / sumMeetingsTarget*100));

                    self.dataSeries.push(mtgPercent);
                    //console.log('BarChartView:parseDataSourceResults:self.dataSeries.length:' + self.dataSeries.length + ":leaderCount:" + self.leaderCount + ":total/target meetings:" + sumTotalMeetings + "/" + sumMeetingsTarget);
                });


                //if (self.dataSeries.length === self.leaderCount) {
                    kendo.ui.progress(self.$("#loadingChart"), false);
                    self.createChart();
                //}
            },
            createChart: function () {

                var self = this;
                var app = App;

                //console.log('BarChartView:createChart:data:labels:' + JSON.stringify(self.dataSeries) + JSON.stringify(self.dataSeriesLabels) );

                var chartOptions = {
                    legend: {
                        display: false,
                        position: 'right'
                    },
                    scales: {
                        yAxes: [{
                            display: true
                        }],
                        xAxes: [{
                            //height: 120,
                            display: true,
                            position: 'bottom'
                        }]
                    },
                    maintainAspectRatio: false,
                    responsive: true
                };

                // Create chart
                var ctx = self.$("#barChart").get(0).getContext("2d");

                var chartData = {
                    datasets:  [
                        {
                            label: "Team Meeting Percentage (%)",
                            lineTension: 0.1,
                            backgroundColor: "rgba(160, 208, 224,0.4)",
                            borderColor: "rgba(160, 208, 224,1)",
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            fillColor: "rgba(160, 208, 224,0.9)",
                            strokeColor: "rgba(160, 208, 224,0.8)",
                            pointColor: "#3b8bba",
                            pointStrokeColor: "rgba(160, 208, 224,1)",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(160, 208, 224,1)",
                            data: self.dataSeries
                        }
                    ],
                    labels: self.dataSeriesLabels
                };

                self.barChart = new Chart(ctx, {
                    type: 'bar',
                    data: chartData,
                    options: chartOptions
                });

                self.barChart.resize();
            },
            displayNoDataOverlay: function () {
                ////console.log('ProjectGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('.tile-content').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                ////console.log('ProjectGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('.tile-content').css('display', 'block');
            },
            onResize: function () {
                //console.log('BarChartView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('BarChartView:resize');

                //this.scatterChart.resize();
                //this.$('#barChart').height(this.$el.parent().height() - 15);

            },
            remove: function () {
                //console.log('---------- BarChartView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                // Destroy chart
                //this.scatterChart.clear();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });