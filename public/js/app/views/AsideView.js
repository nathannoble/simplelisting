define( ['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/aside','adminLTE'],
    function(App, Backbone, Marionette, $, template ) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend( {
            events:{
                //'click #toggleBannerButton': 'onToggleBannerButtonClicked',

            },
            template: template,
            initialize: function() {
                //console.log('AsideView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);
            },
            onShow: function(){
                //console.log('AsideView:onShow');

                var self = this;

                this.$('.manager').css('display', 'block');
                this.$('.executive').css('display', 'block');

                self.userType = parseInt(App.model.get('userType'),0);
                if (self.userType === 1) {
                    //hide manager and executive
                    this.$('.manager').css('display', 'none');
                    this.$('.executive').css('display', 'none');
                } else if (self.userType === 2) {
                    //hide executive
                    this.$('.executive').css('display', 'none');
                }
            },
            _pageView: function (tab) {
                var path = Backbone.history.getFragment();
                var prefix = "";
                if (App.mobile) {
                    if (App.phone)
                        prefix = 'phone-';
                    else
                        prefix = 'tablet-';
                }
                ga('send', 'pageview', {page: prefix + "/" + path + "#" + tab});
            },
            onResize: function () {
                //console.log('AsideView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('AsideView:resize');

                var width = $(window).width();
                //if (width < 768){
                //    this.$("#toggleBannerButton").removeClass("pull-right");
                //    this.$("#toggleBannerButton").addClass("pull-left");
                //    this.$("#toggleFullScreenButton").removeClass("pull-right");
                //    this.$("#toggleFullScreenButton").addClass("pull-left");
                //    this.$("#toggleFullScreenButton").css('margin','-25px 10px 0px 45px');
                //}
                //else{
                //    this.$("#toggleBannerButton").removeClass("pull-left");
                //    this.$("#toggleBannerButton").addClass("pull-right");
                //    this.$("#toggleFullScreenButton").removeClass("pull-left");
                //    this.$("#toggleFullScreenButton").addClass("pull-right");
                //    this.$("#toggleFullScreenButton").css('margin','-25px 45px 0px 10px');
                //}
            },
            remove: function () {
                //console.log('---------- AsideView:remove ----------');

                // Turn off events
                $(window).off("resize", this.onResize);
                $(document).off("webkitfullscreenchange mozfullscreenchange fullscreenchange", this.onFullScreenChange);

                // Remove view events setup in events configuration
                this.undelegateEvents();


                // Remove App event listeners
                // TODO: Add your code, if applicable

                Backbone.View.prototype.remove.apply(this, arguments);
            }

        });
    });