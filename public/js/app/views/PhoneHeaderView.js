define( ['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/phoneHeader','views/SellerView'],
    function(App, Backbone, Marionette, $, template,SellerView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend( {
            events: {
                'click .sign-me-up': 'onSignMeUp',
                'click #menu-toggle': 'onToggleMenuClicked'
            },
            template: template,
            initialize: function() {
                console.log('PhoneHeaderView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);


            },
            onRender: function(){
//                //console.log('---------- PhoneHeaderView:onRender ----------');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.

                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                //console.log('PhoneHeaderView:onShow');

                var self = this;

                this.resize();

            },
            onToggleMenuClicked: function(e){
                console.log('PhoneHeaderView:onToggleMenuClicked');

                e.preventDefault();

                //if ($(window).width() > (screenSizes.sm - 1)) {
                if ($("body .navbar-collapse").hasClass('collapse')) {
                    $("body .navbar-collapse").removeClass('collapse').trigger('collapsed.pushMenu');
                } else {
                    $("body .navbar-collapse").addClass('collapse').trigger('expanded.pushMenu');
                }

            },
            onSignMeUp: function(){
                console.log('PhoneHeaderView:onSignMeUp');

                App.modal.show(new SellerView());
            },
            onResize: function () {
                //console.log('PhoneHeaderView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PhoneHeaderView:resize');
            },
            remove: function () {
                //console.log('---------- PhoneHeaderView:remove ----------');

                // Turn off events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }

        });
    });