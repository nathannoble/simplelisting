define(['App', 'backbone', 'marionette', 'jquery', 'views/SellerView', 'hbs!templates/home',
        'jqueryui','gsdk-checkbox', 'gsdk-morphing', 'gsdk-radio', 'gsdk-bootstrapswitch',
        'bootstrap-select', 'bootstrap-datepicker', 'chartist','jquery.tagsinput'],

    //'kendo/kendo.data','kendo/kendo.maskedtextbox'
    function (App, Backbone, Marionette, $, SellerView, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            events: {
                'click .sign-me-up': 'onSignMeUp'
            },
            initialize: function (options) {

                console.log('HomeView:initialize');

                var self = this;

                _.bindAll(this);

                this.videoLoopCount = 0;
                this.editingCalcValue = false;
                
             },
            onRender: function () {
//                //console.log('---------- HomeView:onRender ----------');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.

                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
//                //console.log('---------- HomeView:onShow ----------');

                console.log('HomeView:onShow');

                var self = this;
                var app = App;

                // Subscribe to browser events
                $(window).on('resize', this.onResize);
                $(window).on('scroll', this.onScroll);

                //this.myVideo = document.getElementById('background-video');//$('#background-video')[0];
                //$(this.myVideo).bind('ended', self.onEnded);

                //Setup the container
                this.resize();
                
                $('#card-product-carousel').carousel({
                      interval: 5000
                });
                
                // CK added for savings calculator ----------------------------------------------
                //console.log("HomeView: Slider Config");
                /*$("#slider-input").slider({
                        value: 250000,
                        min: 150000,
                        max: 1000000,
                        step: 1000,
                        orientation: "horizontal",
                        range: "min",
                        animate: true
                });*/
                
                
                /*$("#slider-input").on("slide", function (evt) {
                    // get the value of the slider, set input form value
                    var homeVal = $('#slider-input').slider("value");
                    $("#input-val").val(homeVal);
                    calcSavings(homeVal);
                });*/

                //self.$("#input-val").kendoMaskedTextBox({});

                self.$("#input-val").change(function (evt) {
                    if (self.editingCalcValue === false) {
                        self.editingCalcValue = true;
                        // get value of input form, set value of slider
                        var homeVal = Number($("#input-val").val().replace(",","").replace(/\D+/g, '' ));
                        //$('#slider-input').slider("value", homeVal);
                        calcSavings(homeVal);
                        var homeValString = app.Utilities.numberUtilities.getFormattedNumberWithRounding(homeVal);
                        $("#input-val").val(homeValString);
                        console.log("HomeView:onShow:input-val:onchange:" + homeValString);
                        self.editingCalcValue = false;
                    }
                });
                
                var calcSavings = function(homeVal) {
                    //console.log(homeVal);
                    var traditionalVal = homeVal * 0.06;
                    var simpleVal = (homeVal * 0.01) + 999;
                    if (simpleVal < 4499) {
                        simpleVal = 4499;
                    }
                    simpleVal = simpleVal;
                    var saveVal = traditionalVal - simpleVal;
                    //console.log(traditionalVal, simpleVal, saveVal);
                    /*$("#output-traditional").val(traditionalVal);
                    $("#output-simple").val(simpleVal);
                    $("#output-save").val(saveVal);*/
                    $("#output-traditional").html("$ " + app.Utilities.numberUtilities.getFormattedNumberWithRounding(traditionalVal));
                    $("#output-simple").html("$ " + app.Utilities.numberUtilities.getFormattedNumberWithRounding(simpleVal));
                    $("#output-save").html("$ " + app.Utilities.numberUtilities.getFormattedNumberWithRounding(saveVal));
                };
                //$("#calcInput")
                
                // For Facebook Pixel conversion tracking
                $( '#SaveButton' ).click(function() {
                    fbq('track', 'AddToCart', {
                        content_ids: ['1234'],
                        content_type: 'product',
                        value: 2.99,
                        currency: 'USD'
                    });
                });
                
                // CK added end ------------------------------------------------------------------

                
            },
            parallax: function (e) {
                var current_scroll = $(window).scrollTop();
                //console.log('HomeView:parallax:current_scroll:' + current_scroll);


                var oVal = $(window).scrollTop() / 3;
                var bigImage = $('.parallax-image').find('img');
                bigImage.css('top', oVal);
            },
            onEnded: function () {
                console.log('HomeView:onEnded:');
                //console.log('HomeView:onEnded:' + JSON.stringify(e));

                var self = this;

                /*
                if (this.videoLoopCount < 4) {
                    this.videoLoopCount += 1;
                    // get the active source and the next video source.
                    // I set it so if there's no next, it loops to the first one
                    //var activesource = $("#background-video source.active");
                    //var nextsource = $("#background-video source.active + source") || $("#background-video source:first-child");
                    var activesource = document.querySelector("#background-video source.active");
                    var nextsource = document.querySelector("#background-video source.active + source") || document.querySelector("#background-video source:first-child");


                    // deactivate current source, and activate next one
                    activesource.className = "";
                    nextsource.className = "active";

                    // update the video source and play

                    self.myVideo.src = nextsource.src;
                    self.myVideo.play();
                    //self.$(self.myVideo).on('ended', self.onEnded);

                }
                */
            },
            onSignMeUp: function(){
//                console.log('HomeView:onSignMeUp');

                App.modal.show(new SellerView());
            },
            onScroll: function () {
                //console.log(' HomeView:onScroll:gsdk:' + gsdk);
                if (!App.mobile && !App.phone) {
                    if (this.gsdk !== null) {
                        gsdk.checkScrollForTransparentNavbar();
                        this.resize();
                    }
                }
            },
            onResize: function () {
                //console.log(' HomeView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                // Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log(' HomeView:resize');

                var height = $(window).height();
                var width = $(window).width();

                var calculatedHeight = height;
                var minHeight = 600;  // 600 for live site
                var headerHeight = 145;

                if (calculatedHeight < minHeight)
                    calculatedHeight = minHeight;
                this.$(".home-container").height(calculatedHeight);

                var scrollHeight = $("#sl")[0].scrollHeight;
                var clientHeight = $("#sl")[0].clientHeight;

                this.gsdk = null;

                if (gsdk && gsdk !== undefined && gsdk !== null) {
                    this.gsdk = gsdk;
                }

                if (width >= 768) {
                    this.parallax();
                }
            },
            remove: function () {
                //console.log('-------- HomeView:remove --------');

                // Remove window events
                $(window).off('resize', this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
