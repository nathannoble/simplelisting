define( ['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/footer', 'views/BreadcrumbView'],
    function(App, Backbone, Marionette, $, template, BreadcrumbView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend( {
            regions:{
                breadcrumbRegion:"#breadcrumb-region"
            },
            template: template,
            onShow: function(){
                //console.log('FooterView:onShow');

                this.breadcrumbRegion.show(new BreadcrumbView());
            },
            updateBreadcrumb: function(){
                //console.log('FooterView:updateBreadcrumb');

                this.breadcrumbRegion.show(new BreadcrumbView());
            }
        });
    });