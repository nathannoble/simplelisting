define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/breadcrumb'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template:template,
            initialize: function(){
                //console.log('BreadcrumbView:initialize');

                // Subscribe to App events
                //this.listenTo(App.vent, App.Events.ModelEvents.BreadcrumbChanged, this.onBreadcrumbChanged, this);
            },
            onRender: function(){
                //console.log('BreadcrumbView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                //console.log('BreadcrumbView:onShow');

                this.onBreadcrumbChanged();
            },
            onBreadcrumbChanged: function(){
                //console.log('BreadcrumbView:onBreadcrumbChanged');

                this.$el.empty();

                var crumbs = App.model.get('breadcrumbs');
                for (var i = 0; i < crumbs.length; i++)
                {
                    var crumb = crumbs[i];
                    var displayParts = crumb.match(/[A-Z][a-z]+/g);
                    var displayCrumb = "";
                    if (displayParts) {
                        for (var j = 0; j < displayParts.length; j++) {
                            displayCrumb += displayParts[j] + " ";
                        }
                        if (displayCrumb.length > 0)
                            displayCrumb = displayCrumb.substring(0, displayCrumb.length - 1);

                        if (i == crumbs.length - 1)
                            this.$el.append('<a class="current" href="#{0}">{1}</a>'.format(crumb.toLowerCase(), displayCrumb));
                        else
                            this.$el.append('<a href="#{0}">{1}</a>'.format(crumb.toLowerCase(), displayCrumb));
                    }
                }
            }
        });
    });