define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/seller', 'jquery.cookie',
        'kendo/kendo.data', 'kendo/kendo.combobox', 'kendo/kendo.maskedtextbox', 'kendo/kendo.dropdownlist'], //'kendo/kendo.datepicker'
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            events: {
                'click #SaveButton': 'saveButtonClicked',
                //'click #NextButton': 'nextButtonClicked',
                'click #CloseButton': 'closeButtonClicked',
                'click #CloseButton2': 'closeButtonClicked'
            },
            initialize: function (options) {
                //console.log('SellerView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;

                this.options = options;

            },
            onRender: function () {
                //console.log('SellerView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- SellerView:onShow --------");
                var self = this;

                //$("#birthDate").kendoDatePicker({
                //    value: empRecord.birthDate
                //});

                this.$('#CloseButton2').hide();

                self.$(".k-textbox").kendoMaskedTextBox();
                self.$("#phone").kendoMaskedTextBox({
                    mask: "(000) 000-0000"
                });
                self.$("#zip").kendoMaskedTextBox({
                    mask: "00000"
                });
                //self.$(".k-numerictextbox").kendoNumericTextBox();

                self.$("#firstName").data("kendoMaskedTextBox").value("");
                self.$("#lastName").data("kendoMaskedTextBox").value("");
                self.$("#email").data("kendoMaskedTextBox").value("");
                self.$("#phone").data("kendoMaskedTextBox").value("");


                self.$("#street").data("kendoMaskedTextBox").value("");
                self.$("#city").data("kendoMaskedTextBox").value("");
                //self.$("#state").data("kendoMaskedTextBox").value("");
                self.$("#zip").data("kendoMaskedTextBox").value("");

                //self.$("#additionalInfo").data("kendoMaskedTextBox").value("");
                //self.$("#additionalInfo2").data("kendoMaskedTextBox").value("");

                var stateList = [
                    {text: 'California', value: "California"},
                    {text: 'Oregon', value: "Oregon"},
                    {text: 'Nevada', value: "Nevada"}
                ];

                self.$("#state").kendoDropDownList({
                    dataValueField: "value",
                    dataTextField: "text",
                    dataSource: stateList,
                    value: "California"
                });

            },
            nextButtonClicked: function (e) {
                //console.log('SellerView:nextButtonClicked');
                this.$('.li-tab').removeClass('active');
                this.$('#liAdvanced').addClass('active');
            },
            saveButtonClicked: function (e) {
                //console.log('SellerView:saveButtonClicked');
                var self = this;

                var firstName = self.$("#firstName").val() || " ";
                var lastName = self.$("#lastName").val() || " ";
                var email = self.$("#email").val() || " ";
                var phone = self.$("#phone").val() || " ";
                var street = self.$("#street").val() || " ";
                var city = self.$("#city").val() || " ";
                var state = self.$("#state").val() || " ";
                var zip = self.$("#zip").val() || " ";
                var company = "simplelisting.com"; //self.$("#company").val() || " ";

                var error = "";
                if (firstName === " " || firstName === null || firstName === undefined) {
                    error = "First Name is a required field.  Please populate and re-submit";
                    self.createAutoClosingAlert($("#sellerError"), "<strong>Warning.</strong> " + error + ".");
                } else if (lastName === " " || lastName === null || lastName === undefined) {
                    error = "Last Name is a required field.  Please populate and re-submit";
                    self.createAutoClosingAlert($("#sellerError"), "<strong>Warning.</strong> " + error + ".");
                } else if (phone === " " || phone === null || phone === undefined) {
                    error = "Phone number is a required field.  Please populate and re-submit";
                    self.createAutoClosingAlert($("#sellerError"), "<strong>Warning.</strong> " + error + ".");
                } else if (email === " " || email === null || email === undefined) {
                    error = "Email is a required field.  Please populate and re-submit";
                    self.createAutoClosingAlert($("#sellerError"), "<strong>Warning.</strong> " + error + ".");
                } else {

                    phone = phone.replace(" ","");
                    phone = phone.replace("(","");
                    phone = phone.replace(")","");
                    phone = phone.replace("-","");

                    this.url = App.config.DataServiceURL + "/rest/CreateSalesforceLead/" + firstName + "/" + lastName + "/" + company +
                        "/" + phone + "/" + email + "/" + street + "/" + city + "/" + state + "/" + zip + "/";

                    this.restDataSource = new kendo.data.DataSource({
                        autobind: false,
                        transport: {
                            type: "odata",
                            read: {
                                url: function () {
                                    //http://199.115.221.114/slservices/rest/CreateSalesforceLead/Louis/Lewis/N%20Squared/909-555-6789/louie%40gmail.com/123%20Main%20St/Redding/California/96002/
                                    return self.url;
                                },
                                type: "GET",
                                dataType: "json"
                            }
                        },
                        requestStart: function (e) {
                            //console.log('SellerView:dataSource:request start:');
                            kendo.ui.progress(self.$("#sellerLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('SellerView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#sellerLoading"), false);
                        },
                        change: function (e) {
                            console.log('SellerView:dataSource:change:');

                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            console.log('SellerView:dataSource:error');
                            kendo.ui.progress(self.$("#sellerLoading"), false);
                        }
                    });

                    this.restDataSource.fetch(function () {
                        var message = "";
                        // Check and report on success/failture
                        console.log('SellerView:dataSource:url:' + self.url);

                        var data = this.data();
                        var count = data.length;

                        if (count > 0) {
                            $.each(data, function (index, value) {
                                message = message + value;
                            });
                            console.log('SellerView:dataSource:response:' + message);

                            if (message === "true") {

                                message = "Your information was received  You will be contacted shortly by a simplelisting.com representative for follow up";
                                self.createSuccessAlert($("#sellerSuccess"), "<strong>Success!</strong> " + message + ".");

                            } else if (message.indexOf("duplicate record") >= 0) {
                                message = "Your information has been received. You will be contacted shortly by a simplelisting.com representative for follow up";
                                self.createSuccessAlert($("#sellerSuccess"), "<strong>Success.</strong> " + message + ".");
                            }
                            else {
                                // show result back to user - error in creating
                                message = "There was a problem with your submission: " + message;
                                self.createAutoClosingAlert($("#sellerError"), "<strong>Warning.</strong> " + message + ".");
                            }
                        }
                    });
                }

            },
            createAutoClosingAlert: function (selector, html) {
                //console.log('SellerView:createAutoClosingAlert');
                var self = this;

                //this.$('#AllButtons').hide();
                this.$(selector).html(html);
                this.$(selector).css("opacity", 1);
                this.$(selector).css("display", "block");
                var alert = $(selector).addClass("alert");

                window.setTimeout(function () {
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                        self.$('#AllButtons').show();
                    });
                }, 10000);
            },
            createSuccessAlert: function (selector, html) {
                console.log('SellerView:createSuccessAlert');
                var self = this;

                this.$('#SaveButton').hide();
                //this.$('#NextButton').hide();
                this.$('#CloseButton2').show();
                this.$(selector).html(html);
                //this.$(selector).html('<p>' + html + '</p><p><button id="CloseSuccessMessage" type="button" class="btn btn-success">Close</button></p>' );
                this.$(selector).css("opacity", 1);
                this.$(selector).css("display", "block");
                var alert = $(selector).addClass("alert");
                //this.$('#CloseSuccessMessage').on('click', this.closeButtonClicked);
            },
            closeButtonClicked: function (e) {
                console.log('SellerView:closeButtonClicked');
                this.close();
            },
            displayNoDataOverlay: function () {
                //console.log('SellerView:displayNoDataOverlay');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('SellerView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('SellerView:resize');

            },
            remove: function () {
                //console.log("-------- SellerView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);


                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });