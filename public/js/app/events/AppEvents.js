define(["jquery", "backbone",  "custom/Class"],
    function($, Backbone, Class) {

        return Class.extend({
            initialize: function(options) {

            },
            // Model Events - events that are triggered by a state change in the model
            // In this scenario we never want these events firing without values persisted
            // on the model...for more details see AppModel.js
            ModelEvents: {

                // Login
                UserIdChanged: "UserIdChanged",
                UserKeyChanged: "UserKeyChanged",
                UserLoginChanged: "UserLoginChanged",
                UserTypeChanged: "UserTypeChanged",
                UserFirstNameChanged: "UserFirstNameChanged",
                UserLastNameChanged: "UserLastNameChanged"
            }
        });
    });