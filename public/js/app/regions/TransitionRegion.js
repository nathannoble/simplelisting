//https://github.com/marionettejs/backbone.marionette/issues/320#issuecomment-9746319
define(['jquery', 'backbone', 'marionette'],
    function ($, Backbone) {
        return Backbone.Marionette.Region.extend({
            el: "#main-region",
            show: function(view){
                console.log('TransitionRegion:show');

                this.ensureEl();
                view.render();

                this.close(function() {
                    if (this.currentView && this.currentView !== view) { return; }
                    this.currentView = view;

                    this.open(view, function(){
                        if (view.onShow){view.onShow();}
                        view.trigger("show");

                        if (this.onShow) { this.onShow(view); }
                        this.trigger("view:show", view);
                    });
                });

            },

            close: function(cb){
                console.log('TransitionRegion:close');

                var view = this.currentView;
                delete this.currentView;

                if (!view){
                    if (cb){ cb.call(this); }
                    return;
                }

                var that = this;
                view.transitionOut(function(){
                    if (view.close) { view.close(); }
                    that.trigger("view:closed", view);
                    if (cb){ cb.call(that); }
                });

            },

            open: function(view, callback){
                console.log('TransitionRegion:open');

                var that = this;
                this.$el.html(view.$el.hide());
                view.transitionIn(function(){
                    callback.call(that);
                });
            }
        });
    });