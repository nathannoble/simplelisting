define( ['App', 'backbone', 'marionette', 'jquery', 'underscore'],
    function(App, Backbone, Marionette, $, _) {
        return Backbone.Marionette.Region.extend({
            el: "#modal",

            constructor: function(){
                console.log('ModalRegion:constructor');

                _.bindAll(this);

                Backbone.Marionette.Region.prototype.constructor.apply(this, arguments);

                this.on("show", this.showModal, this);
            },

            getEl: function(selector){
                console.log('ModalRegion:getEl');

                var $el = $(selector);

                return $el;
            },

            showModal: function(view){
                console.log('------------ ModalRegion:showModal');

                view.on("close", this.hideModal, this);

                this.$el.modal();
            },

            hideModal: function(){
                console.log('ModalRegion:hideModal');

                this.$el.modal('hide');
            }
        });
    });