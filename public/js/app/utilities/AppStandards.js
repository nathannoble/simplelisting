define(["jquery", "backbone",  "custom/Class"],
    function($, Backbone, Class) {

        return Class.extend({
            initialize: function(App) {
                _.bindAll(this);

                this.Application = App;

                this.colorDataDate = null;

                this.colorSelected = null;

                // Budget Classification
                this.colorBudgetClass1 = null;
                this.colorBudgetClass2 = null;
                this.colorBudgetClass3 = null;
                this.colorBudgetClass4 = null;
                this.colorBudgetClass5 = null;

                // TODO: Neutral/Default Deconflict
                this.colorDefault = null;
                this.colorDefaultLight = null;
                this.colorDefaultVeryLight = null;
                this.colorDefaultBright = null;
                this.colorNeutral = null;
                this.colorNeutralLight = null;
                this.colorBad = null;
                this.colorBadLight = null;
                this.colorBadVeryLight = null;
                this.colorBadBright = null;
                this.colorGood = null;
                this.colorGoodLight = null;
                this.colorGoodVeryLight = null;
                this.colorGoodBright = null;
                this.colorWarning = null;
                this.colorWarningLight = null;
                this.colorWarningVeryLight = null;
                this.colorWarningBright = null;

                this.colorCPIPeriod = null;
                this.colorCPICummulative = null;
                this.colorSPIPeriod = null;
                this.colorSPICummulative = null;

                this.colorBaselinePeriod = null;
                this.colorBaselineCummulative = null;
                this.colorActualPeriod = null;
                this.colorActualCummulative = null;
                this.colorEarnedPeriod = null;
                this.colorEarnedCummulative = null;
                this.colorForecastPeriod = null;
                this.colorForecastCummulative = null;

            }
        });
    });