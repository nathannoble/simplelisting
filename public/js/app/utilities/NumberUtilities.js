define(["jquery", "backbone",  "custom/Class"],
    function($, Backbone, Class) {

        return Class.extend({
            initialize: function (App) {
                _.bindAll(this);
            },
            getFormattedFinancialNumberWithPrecision: function (num, precision) {

                if (num > 1000000000) {
                    num = num / 1000000000;
                    if (num.toString().split('.')[0].length >= precision) {
                        num = kendo.toString(num, "c0") + "B";
                    }
                    else {
                        num = kendo.toString(num, "c1") + "B";
                    }
                }
                else if (num > 1000000) {
                    num = num / 1000000;
                    if (num.toString().split('.')[0].length >= precision) {
                        num = kendo.toString(num, "c0") + "M";
                    }
                    else {
                        num = kendo.toString(num, "c1") + "M";
                    }
                }
                else if (num > 1000) {
                    num = num / 1000;
                    if (num.toString().split('.')[0].length >= precision) {
                        num = kendo.toString(num, "c0") + "K";
                    }
                    else {
                        num = kendo.toString(num, "c1") + "K";
                    }
                }

                return num;
            },
            getFormattedFinancialNumber: function (num) {
                if (num > 1000000) {
                    return kendo.toString(num / 1000000, 'c1') + " M";
                }
                else {
                    return kendo.toString(num / 1000, 'c0') + " K";
                }
            },
            getFormattedNumberWithRounding: function (num) {
                // Ex: 1,204,168
                return Math.round(num).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            },
            getFormattedNumberWithTwoPlaceRounding: function (num) {
                // Ex: 1,204,168.41
                return parseFloat(num).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            },
            getFormattedNumberWithOnePlaceRounding: function (num) {
                // Ex: 1,204,168.4
                return parseFloat(num).toFixed(1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            },
            getYearFromDateFilter: function (text) {
                var year = null;
                if (text !== null && text !== undefined && text !== "") {
                    //console.log("NumberUtilities:getYearFromDateFilter:text" + text);
                    year = parseInt(text.substr(text.length - 4, text.length),0);
                }
               return year;
            },
            getMonthFromDateFilter: function(text){
                var month = null;
                if (text !== null && text !== undefined && text !== "") {
                    month = parseInt(text.substring(0, 2), 0);
                }
                return month;
            },
            getStartDateFromDateFilter: function(text){
                var startDate = null;
                if (text !== null && text !== undefined && text !== "") {
                    startDate = text.substring(0, 10);
                }
                return startDate;

            },
            getEndDateFromDateFilter: function(text){

                var endDate = null;
                if (text !== null && text !== undefined && text !== "") {
                    endDate = text.substr(text.length - 10);
                }
                return endDate;

            }
        });
    });