define(["jquery", "backbone",  "custom/Class"],
    function($, Backbone, Class) {

        return Class.extend({
            initialize: function(App) {
                _.bindAll(this);



            },
            getRandomColor: function(){

                var letters = '0123456789ABCDEF'.split('');
                var color = '#';
                for (var i = 0; i < 6; i++ ) {
                    color += letters[Math.floor(Math.random() * 16)];
                }
                return color;

            },
            displayBorders: function(selector, delay) {
                console.log('LayoutDebugger:displayBorders');

                if (selector === null)
                    selector = '*';
                if (delay === null)
                    delay = 0;

                var self = this;
                setTimeout(function(selector){
                    $('*').each(function() {
                        // Note: There is an issue with setting border color in this manor i.e. it will not
                        // overwrite preset borders which for debugging may or may not be desired
                        $( this ).css('border', '1px solid ' + self.getRandomColor());
                    });
                }, delay);
            }
        });
    });