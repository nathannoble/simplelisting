define(['App', 'backbone', 'marionette', 'views/PhoneHeaderView', 'views/FooterView', 'views/HomeView'],
    function (App, Backbone, Marionette, HeaderView, FooterView, HomeView) {
        return Backbone.Marionette.Controller.extend({
            initialize: function (options) {

                $("body").css('overflow', '');
                $("#sl").removeAttr('style');
                if (App.mobile) {
                    $("#loading-error-region").empty();
                    document.ontouchmove = function (e) {
                        return true;
                    };
                }

                // Setup the header and footer
                //$("#loading-error-region").empty();
                //App.headerRegion.show(new HeaderView());
                //App.asideRegion.show(new AsideView());
                //App.footerRegion.show(new FooterView());

            },
            //gets mapped to in AppRouter's appRoutes
            index: function () {
                console.log('TabletController:index');

                // Update containers for scrolling and background
                //this.resetMarkupContainers();
                //$("body").css('overflow-y', 'auto');
                //$("body").css('overflow-x', 'hidden');
                //$("html").css('background-color', '#FFFFFF'); // This ensures the background for overflow elements are white as well
                //$("#sl").css('background-color', '#FFFFFF');

                // Reset the layout
                //this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                //App.asideRegion.reset();
                App.headerRegion.show(new HeaderView());
                //App.footerRegion.reset();
                App.mainRegion.show(new HomeView()); //HomeView());

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            calculator: function () {
                console.log('TabletController:calculator');
                $.when(this.index()).done(function (e) {
                    $('nav[role="navigation"]').removeClass('navbar-transparent');
                    $('html, body').animate({
                        scrollTop: $("#calculator").offset().top
                    }, 1000);
                });

            },
            about: function () {
                console.log('TabletController:about');
                $.when(this.index()).done(function (e) {
                    $('nav[role="navigation"]').removeClass('navbar-transparent');
                    $('html, body').animate({
                        scrollTop: $("#about").offset().top
                    }, 1000);
                });
            },
            resetMarkupContainers: function () {

                $("body").css('overflow-x', '');
                $("body").css('overflow-y', '');
                $("body").css('overflow', '');
                $("html").removeAttr('style');
                $("#sl").removeAttr('style');

                $("html,body").scrollTop(0);
            },
            updateNavBar: function (route) {

                if (route === '')
                    route = 'index';

                if (route.indexOf('?') > -1) {
                    route = route.slice(0, route.indexOf('?'));
                }
                $el = $('#nav-' + route);

                // If current route is highlighted, we're done.
                //if (route != 'esa_app') {
                //    if ($el.hasClass('current_page_item')) {
                //        return;
                //    } else {
                //        // Unhighlight active tab.
                //        $('#main-menu li.current_page_item').removeClass('current_page_item');
                //        // Highlight active page tab.
                //        $el.addClass('current_page_item');
                //    }
                //}
                //else {
                //    if ($el.hasClass('current_sub_page_item')) {
                //        return;
                //    } else {
                //        // Unhighlight active tab.
                //        $('#main-menu li.current_sub_page_item').removeClass('current_sub_page_item');
                //        $('#main-menu li.current_page_item').removeClass('current_page_item');
                //
                //        // Highlight active page tab.
                //        $el.addClass('current_sub_page_item');
                //    }
                //}

                // Highlight the menu if applicable...
                //console.log(route);
                //route === 'dashboard' || route === 'projectfilters' ||
                if (route === 'esa_app') {
                    if ($('#nav-programmenu').hasClass('current_page_item'))
                        return;

                    $('#nav-programmenu').addClass('current_page_item');
                    $('#nav-programmenu-text').addClass('current_page_item_text');

                }
                else {
                    $('#nav-programmenu').removeClass('current_page_item');
                }
            },
            removeLayoutClasses: function () {
                $("#main-region").removeClass("content-wrapper");
                $("#main-region").height(0);
            },
            parseQueryString: function (queryString) {
                var params = {};
                if (queryString) {
                    _.each(
                        _.map(decodeURI(queryString).split(/&/g), function (el, i) {
                            var aux = el.split('='), o = {};
                            if (aux.length >= 1) {
                                var val;
                                if (aux.length == 2)
                                    val = aux[1];
                                o[aux[0]] = val;
                            }
                            return o;
                        }),
                        function (o) {
                            _.extend(params, o);
                        }
                    );
                }
                return params;
            },
            startApp: function (e) {

                var self = this;

                console.log('TabletController:userId:' + this.userId);

                var thisView = this;
            }
        });
    });