define(["jquery", "backbone", "jquery.cookie"],
    function ($, Backbone) {
        // Creates a new Backbone Model class object
        var Model = Backbone.Model.extend({

            // Model Constructor
            Application: null,

            // Default values for all of the Model attributes
            defaults: {
                // Breadcrumbs
                "breadcrumbs": [],

                // App State

                "dateFormat": null,
                // Date format - common values are "MMM dd, yyyy", "dd-MMM-yyyy", "yyyy-MM-ddTHH:mm:ss"

                // Login Info
                userKey: null,
                userId: null,
                userLogin: null,
                userType: null,
                userFirstName:null,
                userLastName:null

            },
            initialize: function (App) {
                // List of events

                // App State
                this.bind("change:userId", this.onUserIdChanged);
                this.bind("change:userKey", this.onUserKeyChanged);
                this.bind("change:userLogin", this.onUserLoginChanged);
                this.bind("change:userType", this.onUserTypeChanged);
                this.bind("change:userFirstName", this.onUserFirstNameChanged);
                this.bind("change:userLastName", this.onUserLastNameChanged);

                // Filters
                this.Application = App;

                // Restore state
                var userId = $.cookie('SL:AppModel:userId');
                if (userId) {
                    console.log('****** AppModel:restoreState:userId:' + userId);
                    if (userId === "null") {
                        this.set("userId", null);

                    }
                    else {
                        this.set("userId", userId);
                    }
                }

                var userKey = $.cookie('SL:AppModel:userKey');
                if (userKey) {
                    console.log('****** AppModel:restoreState:userKey:' + userKey);
                    if (userKey === "null") {
                        this.set("userKey", null);
                    }
                    else {
                        this.set("userKey", parseInt(userKey,0));
                    }
                }

                var userLogin = $.cookie('SL:AppModel:userLogin');
                if (userLogin) {
                    console.log('****** AppModel:restoreState:userLogin:' + userLogin);
                    if (userLogin === "null") {
                        this.set("userLogin", null);
                    }
                    else {
                        this.set("userLogin", userLogin);
                    }
                }

                var userType = $.cookie('SL:AppModel:userType');
                if (userType) {
                    console.log('****** AppModel:restoreState:userType' + userType);
                    if (userType === "null") {
                        this.set("userType", null);
                    }
                    else {
                        this.set("userType", parseInt(userType,0));
                    }
                }

                var userFirstName = $.cookie('SL:AppModel:userFirstName');
                if (userFirstName) {
                    console.log('****** AppModel:restoreState:userFirstName:' + userFirstName);
                    if (userFirstName === "null") {
                        this.set("userFirstName", null);
                    }
                    else {
                        this.set("userFirstName", userFirstName);
                    }
                }

                var userLastName = $.cookie('SL:AppModel:userLastName');
                if (userLastName) {
                    console.log('****** AppModel:restoreState:userLastName:' + userLastName);
                    if (userLastName === "null") {
                        this.set("userLastName", null);
                    }
                    else {
                        this.set("userLastName", userLastName);
                    }
                }
            },
            //**************************************************************
            //* Model Triggers - events fired when properties on the model
            //                   change...these events generally have
            //                   state associated with them
            //**************************************************************

            onUserIdChanged: function () {

                console.log('****** AppModel:onUserIdChanged:userId:' + this.get("userId"));
                $.cookie('SL:AppModel:userId', this.get("userId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.UserIdChanged, this.get('userId'));

            },
            onUserKeyChanged: function () {

                console.log('****** AppModel:onUserKeyChanged:userKey:' + this.get("userKey"));
                $.cookie('SL:AppModel:userKey', this.get("userKey"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.UserKeyChanged, this.get('userKey'));

            },
            onUserLoginChanged: function () {

                console.log('****** AppModel:onUserLoginChanged:userLogin:' + this.get("userLogin"));
                $.cookie('SL:AppModel:userLogin', this.get("userLogin"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.UserLoginChanged, this.get('userLogin'));

            },
            onUserTypeChanged: function () {

                console.log('****** AppModel:onUserTypeChanged:userType:' + this.get("userType"));
                $.cookie('SL:AppModel:userType', this.get("userType"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.UserTypeChanged, this.get('userType'));

            },
            onUserFirstNameChanged: function () {

                console.log('****** AppModel:onFirstNameChanged:userFirstName:' + this.get("userFirstName"));
                $.cookie('SL:AppModel:userFirstName', this.get("userFirstName"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.UserFirstNameChanged, this.get('userFirstName'));

            },
            onUserLastNameChanged: function () {

                console.log('****** AppModel:onUserLastNameChanged:userLastName:' + this.get("userLastName"));
                $.cookie('SL:AppModel:userLastName', this.get("userLastName"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.UserLastNameChanged, this.get('userLastName'));

            },
        //**************************************************************
            //* Event Triggers - trigger all events through this method...
            //*                  it is refined to events that do not have
            //*                  state enforcing App Singleton and MVC rules
            //**************************************************************
            triggerEvent: function (event, model) {

                // Prevent model changes from firing events using this method...i.e.
                // ensure values are changed on the model prior to firing events
                for (var key in this.Application.Events.ModelEvents) {
                    if (key == event) {
                        throw new UserException("Invalid Event Path!");
                    }
                }

                this.Application.vent.trigger(event, model);
            },
            //**************************************************************
            //* Methods
            //**************************************************************


            // Gets called automatically by Backbone when the set and/or save methods are called (Add your own logic)
            validate: function (attrs) {
                //console.log("--------------- AppModel:validate ---------------");
            }

        });

        // Returns the Model class
        return Model;
    }
);