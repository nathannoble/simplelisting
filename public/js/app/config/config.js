require.config({
    baseUrl:"./js/app",
    // 3rd party script alias names (Easier to type "jquery" than "libs/jquery, etc")
    // probably a good idea to keep version numbers in the file names for updates checking
    paths:{
        // Core Libraries
        "jquery":"../libs/jquery",

        "jqueryui":"../libs/jquery-ui.custom.min",
        "jquerymobile":"../libs/jquery.mobile-1.4.4.min",
        "underscore":"../libs/lodash",
        "backbone":"../libs/backbone-min",
        "marionette":"../libs/backbone.marionette",
        "handlebars":"../libs/handlebars",
        "hbs":"../libs/hbs",
        "i18nprecompile":"../libs/i18nprecompile",
        "json2":"../libs/json2",
        //"jasmine": "../libs/jasmine",
        //"jasmine-html": "../libs/jasmine-html",
        //"ext": "../libs/ext-all",
        //"kendo": "../libs/kendo.all.min",

        // Custom Kendo Build (RequireJS AMD)
        "kendo": '../libs/kendo',
        //"kendo.dropdowntreeview": "../libs/plugins/kendo.dropdowntreeview",

        // Plugins
        "backbone.validateAll":"../libs/plugins/Backbone.validateAll",
        "bootstrap":"../libs/plugins/bootstrap-3.3.4.min",
        //"text":"../libs/plugins/text",
        //"jasminejquery": "../libs/plugins/jasmine-jquery",
        //"jquery.iosslider":"../libs/plugins/jquery.iosslider",
        "jquery.cookie":"../libs/plugins/jquery.cookie",
        //"imagesLoaded":  "../libs/plugins/imagesloaded.pkgd.min",
        //"jquery.easing": "../libs/plugins/jquery.easing-1.3",
        "jquery.tagsinput": "../libs/jquery.tagsinput",
        //"jquery.easypiechart": "../libs/plugins/jquery.easypiechart",
        //"jquery.circliful": "../libs/plugins/jquery.circliful.min",
        //"jquery.scrollTo": "../libs/plugins/jquery.scrollTo",
        //"jquery.slimScroll": "../libs/plugins/slimScroll/jquery.slimscroll.min",

        //"jquery.layout":"../libs/plugins/jquery.layout-latest.min",
        //"jquery.tmpl":"../libs/plugins/jquery.tmpl",
        //"jquery.ui.grid":"../libs/plugins/jquery.ui.grid",
        //"jquery.i18n.properties":"../libs/plugins/jquery.i18n.properties-min-1.0.9",
        //"jquery.contextMenu":"../libs/plugins/jquery.contextMenu",
        //"date":"../libs/plugins/date",
        //"canvg":"../libs/plugins/canvg",
        //"html2canvas": "../libs/html2canvas",
        //"jquery.dateFormat": "../libs/plugins/jquery.dateFormat",

        "gsdk-checkbox": "../libs/gsdk-checkbox",
        "gsdk-morphing": "../libs/gsdk-morphing",
        "gsdk-radio": "../libs/gsdk-radio",
        "gsdk-bootstrapswitch": "../libs/gsdk-bootstrapswitch",
        "bootstrap-select": "../libs/bootstrap-select",
        "bootstrap-datepicker": "../libs/bootstrap-datepicker",
        "chartist": "../libs/chartist.min",
        "demo": "../libs/demo",
        "get-shit-done": "../libs/get-shit-done"
        //"sl": "../libs/sl"
    },
    // Sets the configuration for your third party scripts that are not AMD compatible
    shim:{
        // Twitter Bootstrap jQuery plugins
        "bootstrap":["jquery"],

        // jQueryUI
        "jqueryui":["jquery"],

        // jQuery mobile
        "jquerymobile":["jqueryui"],

        // JQuery Plugins
        "jquery.cookie":["jquery"],
        "jquery.iosslider":["jquery"],

        "jquery.easing":["jquery"],
        "jquery.easypiechart":["jquery","jquery.easing"],
        "jquery.circliful": ["jquery"],
        "jquery.scrollTo": ["jquery"],

//        "jquery.XDomainRequest":["jquery"],

        "imagesLoaded":{
            "deps":["jquery"],
            "exports": "imagesLoaded"
        },

        // Backbone
        "backbone":{
            // Depends on underscore/lodash and jQuery
            "deps":["underscore", "jquery"],
            // Exports the global window.Backbone object
            "exports":"Backbone"
        },
        // Marionette
        "marionette":{
            "deps":["underscore", "backbone", "jquery"],
            "exports":"Marionette"
        },
        // Handlebars
        "handlebars":{
            "exports":"Handlebars"
        },
        // Backbone.validateAll plugin that depends on Backbone
        "backbone.validateAll":["backbone"],

        // Kendo
        "kendo/kendo.core": {
            deps: ["jquery"]
        },
        "kendo.dropdowntreeview":{
            "deps": ["kendo/kendo.treeview", "kendo/kendo.dropdownlist", "jquery"],
            "exports": "kendo.dropdowntreeview"
        },
        
        "jasmine": {
            // Exports the global 'window.jasmine' object
            "exports": "jasmine"
        },

        "jasmine-html": {
            "deps": ["jasmine"],
            "exports": "jasmine"
        }

//        "lazyLoader":{
//            "exports": "lazyLoader"
//        }

    },
    // hbs config - must duplicate in Gruntfile.js Require build
    hbs: {
        templateExtension: "html",
        helperDirectory: "templates/helpers/",
        i18nDirectory: "templates/i18n/",

        compileOptions: {}        // options object which is passed to Handlebars compiler
    }
});